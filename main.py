import os
import sys
import zipfile
import logging

logging.basicConfig(
    level=logging.INFO,
    format='%(message)s',
)
logger = logging.getLogger(__name__)

BASE_DIR = 'D:\\'
PACKAGES = 'packages'
LOAD = 'load'

# packages to save
TYPE_FOR_SAVE = [
    'zip',
    'epClarificationDoc',
    'fcsNotificationEA44',
    'fcsNotificationEA615',
    'fcsNotificationCancel',
    'fcsPlacementResult',
    'fcsNotificationZK504',
    'fcsNotificationOK504',
    'fcsNotificationOKU504',
    'fcsNotificationPO615',
    'fcsNotificationZP504',
    'epProlongationCancelEZK',
    'epProlongationCancelEOKOU',
    'fcsNotificationLotCancel',
    'epProlongationEOK',
    'epProlongationEOKOU',
    'epProlongationEZK',
    'fcsNotificationCancelFailure',
    'fcsContractSign',
    'fcs_notificationEFDateChange',
    'fcsNotificationOrgChange',
    'fcsNotificationOKD504',
    'fcsProtocolCancel',
    'fcsProtocolEF1',
    'fcsProtocolEF2',
    'fcsProtocolEF3',
    'fcsProtocolEFInvalidation',
    'fcsProtocolEFSingleApp',
    'fcsProtocolEFSinglePart',
    'fcsProtocolEOK1',
    'fcsProtocolEOK2',
    'fcsProtocolEOK3',
    'fcsProtocolEOKSinglePart',
    'fcsProtocolDeviation',
    'fcsProtocolEvasion',
    'fcsProtocolEOKOU1',
    'fcsProtocolEOKOU2',
    'fcsProtocolEOKOU3',
    'fcsProtocolEOKOUSingleApp',
    'fcsProtocolEZK1',
    'fcsProtocolEZK2',
    'fcsProtocolIP615',
    'fcsProtocolPP615',
    'fcsProtocolPR615',
    'fcsProtocolIPA615',
    'fcsProtocolPRE',
    'fcsProtocolPZP',
    'fcsProtocolVPP',
    'fcsProtocolIZP',
    'epNotificationCancelFailure',
    'epNotificationCancel',
    'epNotificationEZK2020',
    'epNotificationEZT2020',
    'epNotificationEZT2020',
    'fcsClarification',
]
# packages to remove
TYPE_FOR_DELETE = [
    'fcsNotificationINM111',
    'fcsNotificationZA44',
    'epClarificationResult',
    'fcsNotificationZKK44',
    'fcsNotificationZK44',
    'fcsNotificationZKKU44',
    'fcsNotificationOK44',
    'fksNotificationZA44',
    'fksNotificationINM111',
]


def file_move(filename: str, filetype: str) -> None:
    """
    The function to delete files
    """
    try:
        os.rename(os.path.join(BASE_DIR, PACKAGES, LOAD, filename),
                  os.path.join(BASE_DIR, PACKAGES, filetype, filename))
    except FileExistsError:
        logging.info('File %s already exist', filename)
        os.remove(os.path.join(BASE_DIR, PACKAGES, LOAD, filename))


def sort_files() -> None:
    """
    The function for sorting files
    """
    load = os.path.join(BASE_DIR, PACKAGES, LOAD)
    count = {
        'all': 0,
        'zip': 0,
        'sig': 0,
        'xmlrm': 0,
        'xmld': 0,
    }
    while True:
        pref = os.listdir(load)

        for _, _, filenames in os.walk(load):

            for filename in filenames:
                logging.info('In process %s ... ', filename)
                count['all'] += 1
                extention = filename[-3:]

                if extention == 'zip':
                    with zipfile.ZipFile(os.path.join(load, filename)) as filezip:
                        filezip.extractall(os.path.join(load))

                    file_move(filename, extention)
                    count['zip'] += 1
                    logging.info('zip deleted %s ', filename)
                elif extention == 'sig':
                    os.remove(os.path.join(load, filename))
                    count['sig'] += 1
                    logging.info('sig deleted %s ', filename)
                elif extention == 'xml':
                    filetype = filename.split('_')[0]
                    if filetype[:3] == 'fks':
                        filetype = 'fcs' + filetype[3:]

                    if filetype in TYPE_FOR_SAVE:
                        file_move(filename, filetype)
                        count['xmlrm'] += 1
                        logging.info('xml removed %s ', filename)
                    elif filetype in TYPE_FOR_DELETE:
                        os.remove(os.path.join(load, filename))
                        count['xmld'] += 1
                        logging.info('xml deleted %s ', filename)
                    elif filename.find('fcs_notificationEFDateChange') == 0:
                        file_move(filename, 'fcs_notificationEFDateChange')
                        count['xmlrm'] += 1
                        logging.info('xml removed %s ', filename)
        else:
            next = os.listdir(load)
        if not os.listdir(load):
            logging.info('No files in %s ', load)
            break
        else:
            if next == pref:
                logging.info('Неопознанные пакеты %s ', pref)
                break

    logging.info('=' * 20)
    logging.info('total: %s', count['all'])
    logging.info('removed zip: %s', count['zip'])
    logging.info('deleted sig: %s', count['sig'])
    logging.info('removed xml: %s', count['xmlrm'])
    logging.info('deleted xml: %s', count['xmld'])


def create_dir(basedir: str, target: str) -> str:
    '''Create the directory
    :param target: str - name of the directory
    '''
    path = os.path.join(basedir, target)
    try:
        os.makedirs(path)
    except FileExistsError:
        logging.error('Directory %s already exist', path)
    finally:
        return path


def main() -> None:
    packages = create_dir(BASE_DIR, PACKAGES)
    create_dir(BASE_DIR, LOAD)
    for dir in TYPE_FOR_SAVE:
        create_dir(packages, dir)

    sort_files()


if __name__ == '__main__':
    sys.exit(main())
